package StringsFolder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class RestoreString {
    public static String restoreString(String s, int[] indices) {

        Map<Integer,Character> map=new HashMap<>();
        int i=0;
        for(int num :indices)
            map.put(num,s.charAt(i++));

        StringBuilder sb=new StringBuilder();
        TreeMap<Integer,Character> tmap=new TreeMap<>();
        tmap.putAll(map);
        Iterator<Character>iterator=tmap.values().iterator();
        while (iterator.hasNext())
        {
            sb.append(iterator.next());
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        int ind[]={4,5,6,7,0,2,1,3};
        System.out.println(restoreString("codeleet",ind));
    }
}
