package StringsFolder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class ReverseStringSpecialChars {
    public static void main(String[] args) {
        String str="a,b$c";
        StringBuilder sb=new StringBuilder();
        for(int i=str.length()-1;i>=0;i--)
        {
           sb.append(str.charAt(i));
        }
        System.out.println(sb);



//reverse a string
//        StringBuilder sb2= new StringBuilder("Ecommerce");
//        sb2.reverse();
//        System.out.println(sb2.toString());
        String s ="Ecommerce";
        for (int i=s.length()-1;i>=0;i--) {
            System.out.print(s.charAt(i));
        }
        System.out.println();

        //print duplicates from string
        HashMap<Character,Integer> hm=new HashMap<>();
       //  int count=1;
        for (int i=s.length()-1;i>=0;i--) {
            if(hm.containsKey(s.charAt(i))) {
                hm.put(s.charAt(i),hm.getOrDefault(s.charAt(i),1)+1);
            }
            else
            hm.put(s.charAt(i),1);
        }

        for (Map.Entry<Character,Integer>entry: hm.entrySet()) {
            if (entry.getValue() > 1)
                System.out.println(entry.getKey());

        }

//using hashet approach

        HashSet<Character>set=new HashSet<>();
        HashSet<Character>duplicateSet=new HashSet<>();

//        for (char ch:s.toCharArray())
//        {
//            if (set.contains(ch))
//                duplicateSet.add(ch);
//            else
//                set.add(ch);
//
//        }
//        System.out.println("duplicateSet =");
//
       System.out.println("lenght:"+s.length());
        int j=0;
        do {
            char ch=s.charAt(j);
            if (set.contains(ch))
                duplicateSet.add(ch);
            else
                set.add(ch);
            j++;
        }while (j<s.length());


        System.out.println("duplicateSet =");
//
       System.out.println(duplicateSet);
//        Iterator<Character>itr=hashSet.iterator();
//      while (itr.hasNext())
//        {
//            System.out.println(itr);
//        }




    }
}
