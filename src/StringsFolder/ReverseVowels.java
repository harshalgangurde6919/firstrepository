package StringsFolder;

import java.util.Stack;

public class ReverseVowels {
    public static String reverse(String str) {
        Stack<Character> stack = new Stack<>();
        StringBuilder sb = new StringBuilder();
        for (char c : str.toCharArray()) {
            if (c == 'a' || c == 'A' || c == 'e' || c == 'E' || c == 'i' || c == 'I' || c == 'o' || c == 'O' || c == 'u' || c == 'U')
            stack.push(c);
        }
        for ( char c : str.toCharArray()) {
            if (c == 'a' || c == 'A' || c == 'e' || c == 'E' || c == 'i' || c == 'I' || c == 'o' || c == 'O' || c == 'u' || c == 'U') {
                sb.append(stack.pop());
            } else
                sb.append(c);
        }


        return  sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(reverse("Hello"));
    }
}
