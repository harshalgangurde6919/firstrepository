package StringsFolder;

public class AppleChangeCharCase {
    public static void main(String[] args) {


        String str = "aPpLle";
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < str.length() ; i++) {

            char c = str.charAt(i);
            int temp = Integer.valueOf(c);

            //  System.out.println(temp);

            if (temp < 91) {
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(Character.toUpperCase(c));
            }


        }
        System.out.println(sb.toString());

    }
}
