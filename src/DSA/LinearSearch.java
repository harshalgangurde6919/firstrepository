package DSA;

public class LinearSearch {
    static int linearSearch(int a[],int target)
    {
        for(int i=0;i<a.length;i++)
        {
            if(a[i]==target)
                return i;
        }

        return -1;
    }
    static int binarySearch(int a[],int target)
    {
        //{11,13,15,17,21,38,49};
        int left, right ,mid=0;
        left=0;
        right=a.length-1;
        mid=a.length/2;
        System.out.println("left:"+left +" Mid: " +mid +"right: "+ right);
        if(a[mid]==target)
            return mid;
        else if(mid>target) {
            for (int i = 0; i < a.length; i++) {
                if (a[i] == target)
                    return i;
            }
        }

        return -1;
    }


    public static void main(String[] args) {
        int array[]={11,13,15,17,21,38,49};
        int target=17;
        int result=binarySearch(array,target);
        if(result!=-1)
        System.out.println("element found at positon:"+result);
        else
            System.out.println("element not found in array");
    }
}
