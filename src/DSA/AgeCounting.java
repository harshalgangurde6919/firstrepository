package DSA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AgeCounting {

    public static void main(String[] args) {
        try {
            // Send GET request
            URL url = new URL("https://coderbyte.com/api/challenges/json/age-counting");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            // Read response
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();

            // Count items with age >= 50
            String data = response.toString();
            int count = countItemsWithAge50rMore(data);
            StringBuilder res = new StringBuilder(String.valueOf(count));
            String result = res.reverse().toString() + ":4d8rbgepmax";


            // Print the final count
            System.out.println(result);

            // Print final output with ChallengeToken
//            String finalOutput = count + ":4d8rbgepmax";
//            System.out.println("Final Output: " + finalOutput);
//
//            // Remember to replace "xzmpegbrid4" with your ChallengeToken
//            String combinedOutput = finalOutput + ":" + "xzmpegbrid4";
//            System.out.println("Combined Output: " + combinedOutput);

            // Close connection
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int countItemsWithAge50rMore(String data) {
        data = data.replace("{\"data\":\"", "");
        data = data.replace("\"}", "");
        String[] items = data.split(", ");
        int count = 0;
        for (String item : items) {
            String[] parts = item.split("=");
            if (parts[0].equals("age") && Integer.parseInt(parts[1]) >= 50) {
                count++;
            }
        }
        return count;
    }
}
