package DSA;

import java.util.Scanner;

public class ReverseNumber {
    public static void main(String[] args) {
        Scanner input =new Scanner(System.in);
        int n = input.nextInt();

        int reverseNum=0;
        while(n>0)
        {
            int temp=n%10;
            reverseNum=reverseNum*10+temp;
            n=n/10;

        }
        System.out.println("reverse num is:"+reverseNum);
    }
}
