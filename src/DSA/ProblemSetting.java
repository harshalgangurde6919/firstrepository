package DSA;
import java.util.*;

public class ProblemSetting {
    public static int problemSetting(List<Integer> cost, List<List<Integer>> tags) {
        int n = cost.size();
        int m = tags.get(0).size();
        int totalTags = (1 << m);

        // Initialize an array to store the minimum cost required for each subset of tags
        int[] dp = new int[totalTags];
        Arrays.fill(dp, Integer.MAX_VALUE);
        dp[0] = 0; // Base case: No tags covered, so cost is 0

        // Iterate through each subset of tags
        for (int subset = 0; subset < totalTags; subset++) {
            // Iterate through each problem
            for (int i = 0; i < n; i++) {
                int problemTags = 0;
                // Convert the tags of the current problem to a bitmask
                for (int j = 0; j < m; j++) {
                    if (tags.get(i).get(j) == 1) {
                        problemTags |= (1 << j);
                    }
                }
                // Calculate the cost of covering the current subset with the current problem
                int newSubset = subset | problemTags;
                dp[newSubset] = Math.min(dp[newSubset], dp[subset] + cost.get(i));
            }
        }

        return dp[totalTags - 1]; // Return the minimum cost required to cover all tags
    }

    public static void main(String[] args) {
        // Sample Input
        List<Integer> cost = Arrays.asList(10,20);
        List<List<Integer>> tags = Arrays.asList(
                Arrays.asList(1, 0),
                Arrays.asList(0, 1)
        );

        // Sample Output
        System.out.println(problemSetting(cost, tags)); // Output should be 5
    }
}