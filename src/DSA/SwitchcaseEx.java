package DSA;

import java.util.Scanner;

public class SwitchcaseEx {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
//        int day=input.nextInt();
//        switch (day)
//        {
//            case 1-> System.out.println("Monday");
//            case 2-> System.out.println("Tuesday");
//            case 3-> System.out.println("Wednesday");
//            case 4-> System.out.println("Thursday");
//            case 5-> System.out.println("Friday");
//            case 6-> System.out.println("Saturday");
//            case 7-> System.out.println("Sunday");
//            default -> System.out.println("Wrong day");
//
//        }
//
//        switch (day)
//        {
//            case 1,2,3,4-> System.out.println("Weekday");
//            case 6,7-> System.out.println("Weekend");
//
//        }

        // Nested case stmts

         int employee= input.nextInt();
        String dept=input.next();

        switch (employee) {
            case 1:
                System.out.println("Harshal");
                break;
            case 2:
                System.out.println("Akshay");
                break;
            case 3:
                System.out.println("Number 3 ");
                switch (dept) {
                    case "IT":
                        System.out.println("IT Dept");
                        break;
                    case "ELE":
                        System.out.println("Ele dept");
                        break;
                }
                break;
        }


    }
}
