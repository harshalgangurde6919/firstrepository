package DSA;
//Q. Ans = n + (n-1) - (n-2) + (n-3) …. + / - 1

public class SeriesAns {
    public static void main(String[] args) {
        int n = 4;
        int result = calculateSum(n);
        System.out.println("Result: " + result);
    }

    public static int calculateSum(int n) {
        int result = n;
        int sign = 1;
        for (int i = n-1; i > 0; i--) {
            result += sign * i;
            sign *= -1;
        }

        return result;
    }
}
