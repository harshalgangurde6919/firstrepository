package DSA;

import java.util.Scanner;

public class SumOfMatrix {
    static int[][] matrix;
    static int M, N, K;

    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        M = scanner.nextInt();
//        N = scanner.nextInt();
//        K = scanner.nextInt();
//
//        matrix = new int[M][N];
//
//        for (int i = 0; i < M; i++) {
//            for (int j = 0; j < N; j++) {
//                matrix[i][j] = scanner.nextInt();
//            }
//        }
        N = 4;
        M= 4;
        K=2;
        matrix = new int[M][N];
        matrix[0][0] = 1;
        matrix[0][1] = 2;
        matrix[0][2] = 3;
        matrix[0][3] = 4;
        matrix[1][0] = 5;
        matrix[1][1] = 6;
        matrix[1][2] = 7;
        matrix[1][3] = 8;
        matrix[2][0] = 9;
        matrix[2][1] = 1;
        matrix[2][2] = 2;
        matrix[2][3] = 3;
        matrix[3][0] = 4;
        matrix[3][1] = 5;
        matrix[3][2] = 6;
        matrix[3][3] = 7;

        int[][] prefixSum = calculatePrefixSum(matrix);


        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                int sum = getSubmatrixSum(prefixSum, i, j);
                System.out.print(sum + " ");
            }
            System.out.println();
        }


    }

    // Function to calculate the prefix sum of the matrix
    static int[][] calculatePrefixSum(int[][] matrix) {
        int[][] prefixSum = new int[M + 1][N + 1];

        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++) {
                prefixSum[i][j] = matrix[i - 1][j - 1] +
                        prefixSum[i - 1][j] +
                        prefixSum[i][j - 1] -
                        prefixSum[i - 1][j - 1];
            }
        }

        return prefixSum;
    }

    // Function to get the submatrix sum at a given position
    static int getSubmatrixSum(int[][] prefixSum, int row, int col) {
        int r1 = Math.max(0, row - K);
        int r2 = Math.min(M - 1, row + K);
        int c1 = Math.max(0, col - K);
        int c2 = Math.min(N - 1, col + K);

        return prefixSum[r2 + 1][c2 + 1] -
                prefixSum[r2 + 1][c1] -
                prefixSum[r1][c2 + 1] +
                prefixSum[r1][c1];
    }
}