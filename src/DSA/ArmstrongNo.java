package DSA;
/*
Armstrong number is
ex. 153
 1*1*1 + 5*5*5 + 3*3*3 ==153  then it's armstrong no
 i.e addition of cube of all digits in given number is equal to given number then it's armstrong number
 */
//Program to check whether given number is armstrong number or not

import org.openqa.selenium.devtools.v85.dom.model.SetChildNodes;

import java.awt.*;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class ArmstrongNo {


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
     //  int num=input.nextInt();
     //   System.out.println(isArmstrong(num));
        for(int i=100;i<1000;i++)
        {
            if(isArmstrong(i))
            System.out.print(i+" ");
        }
//        int ans=0;
//        int temp=num;
//
//        while(num>0)
//        {
//            int rem=num%10;
//            ans=ans+rem*rem*rem;
//            num=num/10;
//        }
//        if(temp==ans)
//        System.out.println("Armstrong no :"+ans);
//        else System.out.println("Not a armstrong number ");

        //Print all armstrong numbers till specific number
   /*     System.out.println("Enter number till you want to find armstrong numbers ");
        int num = input.nextInt();
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 1; i <= num; i++) {
            int ans = 0;
            int temp=i;
            while (temp > 0) {
                int rem = temp % 10;
                ans = ans + rem * rem * rem;
                temp = temp / 10;
            }
            if(i==ans)
            list.add(ans);

        }
        System.out.println(list);*/

    }

     static boolean isArmstrong(int num) {
        int original=num;
        int ans=0;
         while(num>0)
        {
            int rem=num%10;
            ans=ans+rem*rem*rem;
            num=num/10;
        }
        return original==ans;

    }
}
