package DSA;
import java.util.*;
import java.util.*;

public class Main {
    public static List<String> getLargestSubsequences(String s) {
        int n = s.length();
        char maxChar = 'a'; // Initialize maxChar to the smallest possible character
        List<String> result = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        for (int i = n - 1; i >= 0; i--) {
            maxChar = (char) Math.max(maxChar, s.charAt(i)); // Update maxChar if a larger character is found
            sb.insert(0, maxChar); // Append the maximum character found so far to the result list
            result.add(sb.toString());
        }

        Collections.reverse(result); // Reverse the result list to get the correct order
        return result;
    }

    public static void main(String[] args) {
        String s = "dbca";
        List<String> ans = getLargestSubsequences(s);
        for (String subsequence : ans) {
            System.out.println(subsequence);
        }
        // Output:
        // d
        // dc
        // dca
        // dbca
    }
}
