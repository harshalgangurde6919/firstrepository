package DSA;
import java.util.*;

public class SmallestSubarray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//        int k = scanner.nextInt();
//        int[] arr = new int[n];
//        for (int i = 0; i < n; i++) {
//            arr[i] = scanner.nextInt();
//        }
        int[] arr={5,10,4};
        int k=5;

        int result = findKthSmallestSubarraySum(arr, k);
        System.out.println(result);
    }

    public static int findKthSmallestSubarraySum(int[] arr, int k) {
        int n = arr.length;
        int left = Integer.MAX_VALUE;
        int right = 0;
        for (int i = 0; i < n; i++) {
            left = Math.min(left, arr[i]);
            right += arr[i];
        }

        while (left < right) {
            int mid = left + (right - left) / 2;
            if (countSubarrays(arr, mid) < k) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

    public static int countSubarrays(int[] arr, int target) {
        int count = 0;
        int sum = 0;
        int left = 0;
        for (int right = 0; right < arr.length; right++) {
            sum += arr[right];
            while (sum > target) {
                sum -= arr[left++];
            }
            count += right - left + 1;
        }
        return count;
    }
}
