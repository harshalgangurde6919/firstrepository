package Cucumber.FeatureFiles.StepDefinations;

import Cucumber.FeatureFiles.ClassFiles.FieldsDTO;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.messages.types.Product;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class MyStepdefs {
    WebDriver driver;

    @DataTableType
    public FieldsDTO productEntry(Map<String, String> entry) {
        return new FieldsDTO(entry.get("Product Name"),Integer.parseInt(entry.get("Quantity")),Double.parseDouble(entry.get("Price")));
    }

    @When("^I open chrome browser$")
    public void iOpenChromeBrowser() throws Exception {
        System.setProperty("webdriver.chrome.driver", "/Users/hwgangurde/Documents/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://www.flipkart.com/");
        Thread.sleep(1000);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(5000);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    @And("I open flipcart link")
    public void iOpenFlipcartLink() throws InterruptedException {
        driver.get("https://www.flipkart.com/");
        Thread.sleep(1000);


    }

    @Then("I write test case for adding product to cart")
    public void iWriteTestCaseForAddingProductToCart(List<FieldsDTO>list) throws Throwable{
        System.out.println(list.get(0).getName());
        System.out.println(list.get(0).getPrice());
        System.out.println(list.get(0).getQuantity());





    }

    @And("I write test to add {string} to cart with {string} quantity")
    public void iWriteeTestToAddToCartWithQuantity(String productName,String  quantity) {
        System.out.println(quantity);
        System.out.println(productName);

    }
}
