package package1;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

import java.util.Iterator;
import java.util.Set;

public class NewTest {
    WebDriver driver;
    By fashionLink = By.xpath("//span[text()='Fashion']");
    By fashionAll = By.xpath("//a[text()='All']");
    By shirtOption = By.cssSelector("div[data-id='SHTGTUW5AXABYN73']");
    By addToCart = By.cssSelector("button[class='_2KpZ6l _2U9uOA _3v1-ww']");


    @BeforeTest
    public void beforeTest() throws InterruptedException {
        System.out.println("In Before");
        System.setProperty("webdriver.chrome.driver", "/Users/hwgangurde/Documents/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://www.flipkart.com/");
        Thread.sleep(1000);
        driver.manage().window().maximize();
        Thread.sleep(5000);
    }

    @Test
    public void addToCart() throws InterruptedException {
        System.out.println("In Test");

        driver.findElement(By.cssSelector("span[class='_30XB9F']")).isDisplayed();
        driver.findElement(By.cssSelector("span[class='_30XB9F']")).click();
        Thread.sleep(2000);

        if (driver.findElement(By.cssSelector("a[title='Flipkart']")).isDisplayed()) {
            System.out.println("We are at homepage of flipcart");
        } else
            System.out.println("something went wrong.");

        driver.findElement(fashionLink).isDisplayed();
        driver.findElement(fashionLink).click();
        Thread.sleep(1000);
        driver.findElement(fashionAll).click();
        Thread.sleep(2000);
        driver.findElement(shirtOption).isDisplayed();
        driver.findElement(shirtOption).click();
        Thread.sleep(2000);


        //Iterating to second window
        String mainWindowHandle = driver.getWindowHandle();
        Set<String> allWindowHandles = driver.getWindowHandles();
        Iterator<String> iterator = allWindowHandles.iterator();

        while (iterator.hasNext()) {
            String ChildWindow = iterator.next();
            if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
            }
            Thread.sleep(2000);
        }

        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(addToCart));
        Thread.sleep(2000);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        // Scroll down by pixels (vertical)
        js.executeScript("window.scrollBy(0, 500)"); // Change 500 to the number of pixels you want to scroll
        Thread.sleep(2000);
        if (driver.findElement(addToCart).isEnabled())
            driver.findElement(addToCart).click();


        Thread.sleep(2000);


    }


    @AfterTest
    public void afterTest() {
        System.out.println("In After");
        // driver.quit();
    }

}
