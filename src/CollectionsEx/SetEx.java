package CollectionsEx;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetEx {
    public static void main(String[] args) {
//Hashset
//        Set<String> set = new HashSet<String>();
//        set.add("Nikita");
//        set.add("Akshay");
//        set.add("Harshal");
//        set.add("Bhavesh");
//        set.remove("Harshal");
//        System.out.println(set);
//
//
        Set<String> fruits = new HashSet<>();

        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");
        fruits.add("Apple");// This won't be added as it's a duplicate
        fruits.add("");
        fruits.remove("Banana");
        System.out.println("Fruits:");
        for (String fruit : fruits) {
            System.out.println(fruit);
        }
        Iterator<String> itr=fruits.iterator();
        while (itr.hasNext())
        {
            System.out.println(itr.next());
        }
        //Treeset
        Set<String>ts=new TreeSet<String>();
        ts.add("Akshay");
        ts.add("Nikita");
        ts.add("Harshal");
        ts.add("Bhavesh");

        System.out.println(ts);


    }
}
