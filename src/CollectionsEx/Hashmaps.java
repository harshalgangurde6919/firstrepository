package CollectionsEx;

import org.checkerframework.checker.units.qual.K;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Hashmaps {
    public static void main(String[] args) {

        HashMap<Integer,String> map = new HashMap<>();

        // Adding elements to the Map
        // using standard put() method
        map.put(10,"vishal");
        map.put(30,"sachin");
        map.put(20,"vaibhav");

        // Print size and content of the Map
        System.out.println("Size of map is:- "
                + map.size());


        // Printing elements in object of Map
     //   System.out.println(map);

        System.out.println(map.get("vishal"));
        // Checking if a key is present and if
        // present, print value by passing
        // random element
        if (map.containsKey(10)) {

            // Mapping
            String a = map.get(10);

            // Printing value for the corresponding key
            System.out.println("value for key"
                    + " \"vishal\" is:- " + a);


        for (Map.Entry<Integer, String> e:map.entrySet())
        {
            System.out.println("Key: " + e.getKey()
                    + " Value: " + e.getValue());

        }

        }

        Map<String,Integer>lhm=new LinkedHashMap<>();
        lhm.put("rahul",23);
        lhm.put("Harshal",24);
        lhm.put("Vishal",25);
        lhm.put("Soham",26);
        lhm.put("Ravi",22);
        lhm.remove("Ravi",22);

        System.out.println(lhm);

        Iterator<String>iterator=lhm.keySet().iterator();
       // Iterator<Integer>itr=lhm.values().iterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
        for (String key:lhm.keySet())
        {
            System.out.println(key);
        }
        for (int val:lhm.values())
        {
            System.out.println(val);
        }


    }
}
