package BasicJava;

class Base{
    private int num=10;
    protected int square()
    {
        return num*num;
    }
}
public class JavaInheritence extends Base{

    public static void main(String[] args) {
        JavaInheritence ji=new JavaInheritence();
        System.out.println(ji.square());
    }
}
