package BasicJava;

class test
{


}
public class ExceptionHandling {
    public static void main(String[] args) {
        int ar[] = {10, 20};
        try {
            test ob=null;
            ob.toString();
            int a = 10;
            int b = Integer.parseInt(null);
            //int div = a / b;
            ar[2] = 2;
            //   System.out.println(div);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }
}
