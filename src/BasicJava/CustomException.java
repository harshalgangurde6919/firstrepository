package BasicJava;


class MyException extends Exception{
    public MyException(String s) {
        super(s);
    }
}
public class CustomException {
    public static void main(String[] args) throws MyException {
        try{
            throw new MyException("Custom Exception");
        }
        catch (MyException ex)
        {
            System.out.println("Exception caught ");
        }
        finally {
            System.out.println("we are in finally block ");
        }
    }
}
