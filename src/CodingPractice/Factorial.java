package CodingPractice;

public class Factorial {
    void factorial(int n)
    {
        int fact=1;
        for(int i=1;i<=n;i++)
        {
           fact=fact*i;

        }
        System.out.println(fact);
    }

    int recur(int num)
    {
        if(num==1)
            return 1;
        return num*recur(num-1);

    }
    public static void main(String[] args) {

        Factorial fac=new Factorial();
        fac.factorial(4);
        System.out.println(fac.recur(4));
    }
}
