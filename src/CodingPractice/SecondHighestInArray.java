package CodingPractice;

public class SecondHighestInArray {
    public static void main(String[] args) {
        int a[]={70,20,30,1115,34};
        int secondHighest=0,highest=0;
        for(int i:a)
        {
            if(i>highest)
            {
                secondHighest=highest;
                highest=i;
            }
            else
                if(i>secondHighest)
                    secondHighest=i;

        }
        System.out.println(secondHighest);
    }
}
