package LambdaExpr;

import java.util.*;
import java.util.stream.Collectors;

public class StreamEx2 {
    public static void main(String[] args) {
        List<Integer> l1 = new ArrayList<>();
        l1.add(10);
        l1.add(20);
        l1.add(15);
        l1.add(25);
        l1.add(30);
        l1.add(35);
        Set<Integer>s1=new HashSet<>();
     //Stream functions and use
       // System.out.println(l1.stream().count();

      //  l1.forEach((n)-> System.out.println(n));
      //  System.out.println(l1.stream().findAny());
     //   l1.stream().filter(num->num%2==0).forEach(System.out::println);


        //Stream comparators class

        l1.stream().sorted(Comparator.naturalOrder()).
                forEach(System.out::println);


    }
}
