package LambdaExpr;

import java.util.*;
import java.util.stream.Collectors;

public class LambdaEx {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(9);
        numbers.add(8);
        numbers.add(1);

        numbers.forEach(
                (n)->System.out.println(n)
        );

        List<Integer> ls
                = numbers.stream()
                .filter(i -> i % 2 == 0)
                .collect(Collectors.toList());
        System.out.println(ls);

        numbers.forEach(n-> System.out.println(n));

    }
}
