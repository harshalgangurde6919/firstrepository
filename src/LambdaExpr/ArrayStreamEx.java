package LambdaExpr;

import java.util.Arrays;

public class ArrayStreamEx {
    public static void main(String[] args) {
        int a[]={450,20,30,24};


     Arrays.stream(a)
             .sorted().
             forEach(System.out::println);

    }
}
