package ArraysFolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateTargetArr {
    public static int[] createTargetArray(int[] nums, int[] index) {
        int []tArr=new int [nums.length];
        List<Integer> Al=new ArrayList<>();

        for(int i=0;i<nums.length;i++)
        {
            Al.add(index[i],nums[i]);
        }

        for(int i=0;i<Al.size();i++)
        {
            tArr[i]=Al.get(i);
        }
        return tArr;

    }

    public static void main(String[] args) {
       int[] nums = {0,1,2,3,4};
       int []index = {0,1,2,2,1};
        System.out.println(Arrays.toString(createTargetArray(nums,index)));

    }
}
