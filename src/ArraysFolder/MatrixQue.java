package ArraysFolder;

import java.util.Arrays;

public class MatrixQue {
    public static int  oddCells(int n, int m, int[][] indices) {
        int[][] a = new int[m][n];
        for(int[] ind: indices){
            System.out.println(Arrays.toString(ind));
            for(int j =0; j <n; j++){
                System.out.println(Arrays.deepToString(a));
                a[ind[0]][j]++;
            }
            for(int i =0; i <m; i++){
                a[i][ind[1]]++;
            }
        }
        System.out.println(Arrays.deepToString(a));
        int cnt=0;
        for(int i =0; i <m; i++){
            for(int j =0; j<n;j++){
                if(a[i][j] %2 !=0)
                    cnt++;
            }
        }
        return cnt;

    }
    public static void main(String[] args) {
        int indices[][]={{0,1},
                {1,1}};
        System.out.println( oddCells(3,2,indices));
    }
}
