package ArraysFolder;

public class FindPeak {
    public static int findPeakElement(int[] nums) {

        if (nums.length == 1)
            return 0;

        for (int i = 1; i <= nums.length-1; i++) {
            if (nums[i] > nums[i + 1] && nums[i] > nums[i - 1])
                return i;
            else if (nums[i] < nums[i + 1])
                return i + 1;
            else if (nums[i] < nums[i - 1])
                return i - 1;


        }
        return -1;

    }
    public static int binarysearch(int nums[])
    {
        int start=0;
        int end=nums.length-1;
        while(start<end)
        {
            int mid=start+(end-start)/2;

            if(nums[mid]<nums[mid+1])
                start=mid+1;
            else
                end=mid;
        }

        return start;

    }

    public static void main(String[] args) {
        int arr[] = {1,2,1,3,5,6,4};

        System.out.println(binarysearch(arr));
    }
}
