package ArraysFolder;

public class BinarySearch {
    public static int binarySearch(int array[],int target)
    {
        int start=0;
        int end=array.length-1;

        while(start<=end)
        {
           // int mid=(start+end)/2;//this will not work for larger numbers

            int mid=start+(end-start)/2;
            if(array[mid]>target)
            {
                end=mid-1;
            } else if (array[mid]<target) {
                start=mid+1;

            }
            else {
                return mid;
            }

        }
        return -1;

    }

    public static int orderAgnosticBinarySearch(int array[],int target)
    {
        int start=0;
        int end=array.length-1;
        boolean isAsc=array[start]<=array[end];

        while(start<=end)
        {
            // int mid=(start+end)/2;//this will not work for larger numbers

            int mid=start+(end-start)/2;
            if(array[mid]==target)
                return mid;
            if(isAsc) {
                if (array[mid] > target) {
                    end = mid - 1;
                } else  {
                    start = mid + 1;

                }
            }
            else {
                if (array[mid] < target) {
                    end = mid - 1;
                } else  {
                    start = mid + 1;

                }
            }


        }
        return -1;
    }

    public static int binarysearchtest(int []arr,int target)
    {
        int start=0;
        int end=arr.length-1;

        while (start<=end)
        {
            int mid=start+(end-start)/2;
            if (arr[mid]>target)
            {
                end=mid-1;

            } else if (arr[mid]<target) {
                start=mid+1;
            }
            else
                return mid;
        }

        return -1;
    }

    public static void main(String[] args) {
    //int arr[]={-15,-5,0,2,5,13,19,25};
      //   int arr[]={5,7,10};
        int arr[]={5};
    int target=5;
    int ans=binarysearchtest(arr,target);
    //int ans=orderAgnosticBinarySearch(arr,target);
  //  int ans=binarySearch(arr,target);
        System.out.println(ans);
    }
}
