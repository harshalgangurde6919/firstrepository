package ArraysFolder;

import io.cucumber.java.sl.In;

import java.util.*;

public class RemoveDuplicates {

            public static int removeDuplicates(int[] nums) {
                int j = 1;
                for (int i = 1; i < nums.length; i++) {
                    if (nums[i] != nums[i - 1]) {
                        nums[j] = nums[i];
                        j++;
                    }
                }
                System.out.println(Arrays.toString(nums));
                return j;
            }

            public static int removeDuplicateForSortedArray(int[]nums)
            {
                HashMap<Integer,Integer>hm=new HashMap<>();
                for(int n:nums)
                {
                    if(hm.containsKey(n))
                      hm.put(n, hm.getOrDefault(n,1)+1);
                    else
                        hm.put(n,1);

                }
                ArrayList<Integer> arrayList=new ArrayList<>();
              for( Map.Entry<Integer, Integer>entry:hm.entrySet())
              {
                  arrayList.add(entry.getKey()) ;

              }
                System.out.println(arrayList);
              int size=arrayList.size();
                ListIterator<Integer>list= arrayList.listIterator();
                int j=0;
               while(list.hasNext())
               {
                   nums[j++]= list.next();
               }
                System.out.println("nums:");
               for (int nu:nums)
                   System.out.println(nu);

               Arrays.sort(nums);
                return size;
            }
    public static void main(String[] args) {
        int a[]={ -3,-1,0,0,0,3,3};
        //System.out.println(removeDuplicates(a));
      //  removeDuplicates2(a);
removeDuplicateForSortedArray(a);


    }

    public static int removeDuplicates2(int[] nums) {

        for (int i=0;i< nums.length;i++)
        {
            for (int j = 0; j <nums.length; j++) {
                if(nums[i]<nums[j])
                {
                    int temp=nums[i] ;
                    nums[i]=nums[j];
                    nums[j]=temp;
                }
            }

        }

        for (int i = 0; i < nums.length-2; i++) {
            for (int j = 1; j < nums.length-1; j++) {
                if (nums[i] == nums[j]) {
                    nums[j] = nums[j + 1];
                }
            }

        }

        System.out.println(Arrays.toString(nums));

        return 10;

    }
}
