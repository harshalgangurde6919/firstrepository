package ArraysFolder;

import java.util.Arrays;

public class MoveZeroesEx {
    public static void main(String[] args) {
        int arr[]={0,1,0,3,12};
        moveZeroes(arr);
        System.out.println(Arrays.toString(arr));
    }
    public static void moveZeroes(int[] nums) {

        int left = 0, right = 0;
        System.out.println(nums.length);
        for (int i = 1; right < nums.length; i++) {
            if (nums[right] == 0)
                right++;
            else {
                int temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
                left++;
                right++;
            }
        }


    }
}
