package ArraysFolder.SortingAlgorithms;

import java.lang.reflect.Array;
import java.util.Arrays;


// time complexity O(nlogn) it works on divide and conquer mechanism
public class MergeSortTest {

    public static void Merge(int arr[],int low,int mid,int high)
    {
        int ans[]=new int[(high-low)+1];
        int left=low;
        int right=mid+1;
        int i=0;

        while(left<=mid  &&  high>=right)
        {
            if(arr[left]<=arr[right])
                ans[i++]=arr[left++];
            else
                ans[i++]=arr[right++];
        }

        while(left<=mid ) {
                ans[i++] = arr[left++];
        }

        while( high>=right) {
            ans[i++] = arr[right++];
        }

   //     System.out.println(Arrays.toString(ans));

        for(int j=0,k=low;j< ans.length;j++,k++)
        {
            arr[k]=ans[j];

        }

    }

public static void mergeSort(int arr[],int low,int high)
{
    if(low>=high) return;
    int mid=low+(high-low)/2;
    mergeSort(arr, low, mid);
    mergeSort(arr,mid+1,high);
    Merge(arr,low,mid,high);


}

    public static void main(String[] args) {

        int arr[]={12,5,2,4,11,2};
        mergeSort(arr,0, arr.length-1);
        System.out.println(Arrays.toString(arr));

    }
}
