package ArraysFolder.SortingAlgorithms;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


// time complexity worst O(n^2)
//avg case O(nlogn)

public class QuickSort {
    static int partitionTest(int arr[],int low,int high)
    {
        int i=low;
        int j=high;
        int pivot=arr[low];

        while(i<j)
        {

            while(arr[i]<pivot)
            {
                i++;
            }
            while(arr[j]>pivot) {
                j--;
            }

            if(i<j)
            {
                int temp=arr[i];
                arr[i]=arr[j];
                arr[j]=temp;

            }
        }

        int temp=pivot;
        pivot=arr[j];
        arr[j]=temp;
        return j;

    }

    static void quicksortTest(int array[],int low, int high)
    {

        if(low<high)
        {
            int pivot=partitionTest(array,low,high);
            quicksortTest(array,low,pivot);
            quicksortTest(array,pivot+1,high);

        }


    }





    static int partition(List<Integer> arr, int low, int high) {
        int pivot = arr.get(low);
        int i = low;
        int j = high;

        while (i < j) {
            while (arr.get(i) <= pivot && i <= high - 1) {
                i++;
            }

            while (arr.get(j) > pivot && j >= low + 1) {
                j--;
            }
            if (i < j) {
                int temp = arr.get(i);
                arr.set(i, arr.get(j));
                arr.set(j, temp);
            }
        }
        int temp = arr.get(low);
        arr.set(low, arr.get(j));
        arr.set(j, temp);
        return j;
    }

    static void qs(List<Integer> arr, int low, int high) {
        if (low < high) {
            int pIndex = partition(arr, low, high);
            qs(arr, low, pIndex - 1);
            qs(arr, pIndex + 1, high);
        }
    }
    public static List<Integer> quickSort(List<Integer> arr) {
        // Write your code here.
        qs(arr, 0, arr.size() - 1);
        return arr;
    }

    public static void main(String args[]) {
//        List<Integer> arr = new ArrayList<>();
//        arr = Arrays.asList(new Integer[] {4, 6, 2, 5, 7, 9, 1, 3});
//        int n = arr.size();
//        System.out.println("Before Using quick Sort: ");
//        for (int i = 0; i < n; i++) {
//            System.out.print(arr.get(i) + " ");
//        }
//        System.out.println();
//        arr = quickSort(arr);
//        System.out.println("After quick sort: ");
//        for (int i = 0; i < n; i++) {
//            System.out.print(arr.get(i) + " ");
//        }
//        System.out.println();
//
//


        int array []={4, 6, 2, 5, 7, 9, 1, 3};
        quicksortTest(array,0, array.length-1);
        System.out.println(Arrays.toString(array));
    }

}
