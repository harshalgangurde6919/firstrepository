package ArraysFolder.SortingAlgorithms;

import java.util.Arrays;

public class LeetcodeQuickSortEx {

    public static int partition(int nums[],int low,int high)
    {
        int pivot=nums[low];
        int i=low;
        int j=high;

        while(i<j)
        {
            while(nums[i]<pivot)
            {
                i++;
            }
            while(nums[j]>pivot)
            {
                j--;
            }
            if(i<j)
            {
                int temp=nums[i];
                nums[i]=nums[j];
                nums[j]=temp;

            }

        }
        int temp=pivot;
        pivot=nums[j];
        nums[j]=temp;

        return j ;

    }

    public static void quickSort(int nums[],int low,int high)
    {

        if(low<high)
        {
            int pivot=partition(nums,low,high);
            quickSort(nums,low,pivot);
            quickSort(nums,pivot+1,high);
        }

    }




    public static int[] sortArray(int[] nums) {
        int low=0;
        int high=nums.length-1;
        quickSort(nums,low,high);
        System.out.println(Arrays.toString(nums));


        return nums;

    }

    public static void main(String[] args) {
        int nums[]={5,2,3,1};
        sortArray(nums);
    }
}
