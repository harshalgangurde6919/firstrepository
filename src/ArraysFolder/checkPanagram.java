package ArraysFolder;

import java.util.HashMap;

public class checkPanagram {
    public static boolean checkIfPangram(String sentence) {
        HashMap<Integer, Character> map = new HashMap<Integer, Character>();

        for (int i = 0; i < sentence.length(); i++) {
            int chIndex = (int) (sentence.charAt(i));
            if (!map.containsKey(chIndex)) {
                map.put( chIndex, sentence.charAt(i));
            }
        }
        for (int i = 0; i < 26; i++) {

            if (!map.containsKey(97 + i)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(checkIfPangram("leetcode"));
    }
}
