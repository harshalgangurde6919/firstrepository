package ArraysFolder;

import java.lang.reflect.Array;
import java.util.Arrays;

/*

Given an integer array of nums sorted in non-decreasing order, return an array of the squares of each number,
also sorted in non-decreasing order. Code this in O(n) to solve the problem.
Example:
Input: nums = [-8, -3, 1, 3, 7]
Output: [1, 9, 9, 49, 64]
Explanation: After squaring, the array becomes [64, 9, 1, 9, 49]. After sorting, it becomes [1, 9, 9, 49, 64].
 */
public class ArraySquare {

    public static int binarySearch(int arr[],int target)
    {
        int start=0;
        int end=arr.length-1;
        while(start<=end)
        {
            int mid=start+(end-start)/2;

            if(target>arr[mid])
                start=mid+1;
            else
                if(arr[mid]>target)
                    end=mid-1;
                else
                    return mid;

        }
        return -1;


    }

    public static void main(String[] args) {
//      int[]  nums ={-8, -3, 1, 3, 7};
//      int square[]=new int[nums.length];
//      int i=0;
//      for(int n:nums)
//      {
//          square[i++]=n*n;
//      }
//
//      for (int j=1;j<square.length;j++)
//      {
//          for(int k=0;k<i;k++)
//          {
//              if(square[k]>square[j])
//              {
//                  int temp=square[j];
//                  square[j]=square[k];
//                  square[k]=temp;
//              }
//          }
//
//
//      }
//
//        System.out.println(Arrays.toString(square));


        int arr[]={3,10,44,55};
        System.out.println(binarySearch(arr,55));



    }
}
